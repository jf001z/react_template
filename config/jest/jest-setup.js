import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import 'jest-styled-components';

configure({ adapter: new Adapter() });

jest.mock('react-appinsights', () => {});
jest.mock('decimal.js-light', () => {});
