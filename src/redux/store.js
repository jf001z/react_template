import { createStore, applyMiddleware, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import appSaga from '../sagas';
import appReducer from '../reducers';

// Load initial state if any
const initialState =
  typeof localStorage !== 'undefined' && localStorage['redux-store']
    ? JSON.parse(localStorage['redux-store'])
    : {};

const persistConfig = {
  key: 'root',
  storage,
};

const persistedReducer = persistReducer(persistConfig, appReducer);

function getStore(inState = {}) {
  const sagaMiddleware = createSagaMiddleware();
  const middleWares = [sagaMiddleware];
  const composables = [applyMiddleware(...middleWares)];
  const enhancer = compose(...composables);
  const store = createStore(persistedReducer, inState, enhancer);
  const persistor = persistStore(store);
  sagaMiddleware.run(appSaga);
  // sagaMiddleware.run(loginSaga);
  // store.subscribe(saveState)
  return { store, persistor };
}
// Create store
const { store, persistor } = getStore(initialState);

export { store, persistor };
