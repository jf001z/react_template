import { TEST_ACTION_RESPONSE } from '../actions';

const testReducer = (state = null, action) => {
  switch (action.type) {
    case TEST_ACTION_RESPONSE:
      return action.payload;
    default:
      return state;
  }
};
export default testReducer;
