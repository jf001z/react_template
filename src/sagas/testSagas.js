import { takeEvery, put } from 'redux-saga/effects';
import { TEST_ACTION, TEST_ACTION_RESPONSE } from '../actions';

export default function*() {
  yield takeEvery(TEST_ACTION);
  yield put({ type: TEST_ACTION_RESPONSE, payload: 'This is a test' });
}
