import { all, call } from 'redux-saga/effects';
import testSage from './testSagas';

export default function* appSaga() {
  yield all([call(testSage)]);
}
